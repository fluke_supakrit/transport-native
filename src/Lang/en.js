
export default {

    // Screen Intro
    Intro_guide : "Guide",
    Intro_step_1 : "Step 1",
    Intro_step_1_card_title : "Search for a Trip",
    Intro_step_1_card_body : "Easy booking Select the origin province,\ndestination province, date and time and click search.",
    Intro_step_2 : "Step 2",
    Intro_step_2_card_title : "Choose a Trip",
    Intro_step_2_card_body : "Choose the trip, date and time you want to go.\nWith many special discounts for members.",
    Intro_step_3 : "Step 3",
    Intro_step_3_card_title : "Enter passenger information\nwith special privileges.",
    Intro_step_3_card_body : "Enter passenger information to travel with us.\nWith special privileges, promotion,\n various coupons discounts.",
    Intro_step_4 : "Step 4",
    Intro_step_4_card_title : "Payment",
    Intro_step_4_card_body : "Various payment methods with convenience\nto everyone.",
    Intro_step_5 : "Step 5",
    Intro_step_5_card_title : "Issue tickets",
    Intro_step_5_card_body : "Issue a ticket to use the ticket for your travel.",

    // Screen Login
    Login_username : "Username",
    Login_password : "Password",
    Login_forgotPassword : "Forgot Password",
    Login_register : "Register",
    Login_login : "Login",
    Login_skip : "Skip",


    // Screen Register
    Register_Register : "Register",
    Register_CreateYourAccount : "Create your Account",
    Register_Email : "Email",
    Register_Username : "Username",
    Register_Password : "Password",
    Register_ConfirmPassword : "Confirm your Password",
    Register_PersonalInformation : "Personal Information",
    Register_Title : "Title",
    Register_prefix_mr : "Mr",
    Register_prefix_ms : "Miss",
    Register_Name : "Name",
    Register_LastName : "Last Name",
    Register_Sex : "Sex",
    Register_Male : "Male",
    Register_Female : "Female",
    Register_DateOfBirth : "Date of birth",
    Register_IdentificationNumber : "Identification Number",
    Register_PassportNumber : "Passport Number",
    Register_Mobile : "Mobile",
    Register_Address : "Address",
}