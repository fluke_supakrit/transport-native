export default {

    // Screen Intro
    Intro_guide : "แนะนำการใช้งาน",
    Intro_step_1 : "ขั้นตอนที่ 1",
    Intro_step_1_card_title : "ค้นหาเที่ยวรถ",
    Intro_step_1_card_body : "การจองรถที่แสนง่ายดาย เลือกจังหวัดต้นทาง\nจังหวัดปลายทาง วันเวลา กดค้นหาเที่ยวรถ",
    Intro_step_2 : "ขั้นตอนที่ 2",
    Intro_step_2_card_title : "เลือกเที่ยว",
    Intro_step_2_card_body : "เลือกบริษัท เที่ยวรถ วันเวลา ที่ต้องการไป พร้อมส่วนลด\nพิเศษมากมายสำหรับสมาชิก",
    Intro_step_3 : "ขั้นตอนที่ 3",
    Intro_step_3_card_title : "ป้อนข้อมูลผู้โดยสารพร้อมสิทธิพิเศษ",
    Intro_step_3_card_body : "ป้อนข้อมูลผู้โดยสาร ที่จะร่วมเดินทางไปกับเรา\nพร้อมสิทธิพิเศษ โปรโมชั่นคูปองส่วนลดต่างๆ",
    Intro_step_4 : "ขั้นตอนที่ 4",
    Intro_step_4_card_title : "ชำระเงิน",
    Intro_step_4_card_body : "วิธีการชำระเงินที่หลากหลาย และสะดวกสบาย\nพร้อมอำนวยความสะดวกให้แก่ ทุกท่าน",
    Intro_step_5 : "ขั้นตอนที่ 5",
    Intro_step_5_card_title : "ออกตั๋ว",
    Intro_step_5_card_body : "สามารถออกตั๋วโดยสาร เพื่อนำตั๋วไปใช้\nในการเดินทางของท่านได้เลย",

    // Screen Login
    Login_username : "ชื่อผู้ใช้",
    Login_password : "รหัสผ่าน",
    Login_forgotPassword : "ลืมรหัสผ่าน",
    Login_register : "สมัครสมาชิก",
    Login_login : "เข้าสู่ระบบ",
    Login_skip : "ข้าม",


      // Screen Register
      Register_Register : "สมัครสมาชิก",
      Register_CreateYourAccount : "ข้อมูลสำหรับการเข้าสู่ระบบ",
      Register_Email : "อีเมล",
      Register_Username : "ชื่อผู้ใช้",
      Register_Password : "รหัสผ่าน",
      Register_ConfirmPassword : "กรุณากรอกรหัสผ่านอีกครั้ง",
      Register_PersonalInformation : "ข้อมูลส่วนตัว",
      Register_Title : "คำนำหน้า",
      Register_prefix_mr : "นาย",
      Register_prefix_ms : "นางสาว",
      Register_Name : "ชื่อ",
      Register_LastName : "นามสกุล",
      Register_Sex : "เพศ", 
      Register_Male : "ชาย",
      Register_Female : "หญิง",
      Register_DateOfBirth : "วันเดือนปีเกิด",
      Register_IdentificationNumber : "เลขประจำตัวประชาชน",
      Register_PassportNumber : "เลขหนังสือเดินทาง",
      Register_Mobile : "หมายเลขโทรศัพท์",
      Register_Address : "ที่อยู่",

}