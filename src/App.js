import { StatusBar } from 'expo-status-bar';

import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { StyleSheet } from 'react-native';
import * as Font from 'expo-font';
import { registerRootComponent , AppLoading } from 'expo';

import Router from "./Routes"

// Redux
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './Reducers'
const store = createStore(rootReducer)

function App() {

  const [ fontLoaded , setFontLoaded ] = useState(false)
  useEffect(() => {

    function getFont(){
           Font.loadAsync({
             'Tahoma' : require('../assets/fonts/TahomaRegular.ttf'),
           }).then(() => setFontLoaded(true))
    }

    getFont()

  },[])

      if(fontLoaded){

        return (
          <Provider store={store}>
              <Router />
          </Provider>
        )
      }else{
        return (
          <AppLoading />
        )
      }

}

const styles = StyleSheet.create();

export default registerRootComponent(App);

