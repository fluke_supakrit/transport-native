let init = {

    profile: {
      _id: '',
      accountId: '',
      accountType: '',
      username: '',
      birthdate: '',
      gender: '',
      email: '',
      mobileNumber: '',
      password: '',
      imageProfile: '',
      createBy: '',
      updateBy: '',
      isActive: '',
      createAt: '',
      updateAt: '',
    },
  };
  
  export default (state = init, action) => {
    switch (action.type) {
      case 'UPDATE_USER':
        state.profile = action.payload;
        return state;
      default:
        return state;
    }
  };
  