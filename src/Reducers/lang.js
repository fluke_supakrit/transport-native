

let init = {

    language  : "en"
};
  
  export default (state = init, action) => {
    switch (action.type) {
      case 'CHANGE_LANGUAGE':
        state.language = action.payload
        return state;
      default:
        return state;
    }
};
  