import { combineReducers } from 'redux'
import profile from './profile'
import auth from './auth'
import lang from './lang'

export default combineReducers({
    profile,
    auth,
    lang
})