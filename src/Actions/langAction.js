


export const change_language = (language) => ({
    type: 'CHANGE_LANGUAGE',
    payload : language
})