import React, { useState } from "react";
import { StyleSheet, View, ScrollView, TouchableOpacity } from "react-native";
import { MaterialIcons, Entypo, FontAwesome } from "@expo/vector-icons";
import { color } from "../../Utils";

import {
  DateTimePicker,
  Container,
  KeyboardAvoidingView,
  Header,
  Text,
  TextInput,
  TermConditon,
  Button,
  Picker,
  RadioBtn,
  Switch
} from "../../Components";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { t } from "i18n-js";

export default function SearchScreen({ navigation }) {
  // State
  const [departDate, setDepartDate] = useState("");
  const [noPassenger, setNoPassenger] = useState(0);
  const [roundTrip, setRoundTrip] = useState(false);

  // Event
  const handleOnSelectDepartDate = (selectedDate) => {
    setDepartDate(selectedDate);
  };


  const handleOnSearch = () => {

    navigation.navigate("TicketScreen")
  }

  return (
    <Container>
      <Header title={"Search"} />

      <KeyboardAvoidingView>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
          <Text style={styles.topic}>From</Text>
          <TextInput
            leftIcon={
              <Entypo
                name="location-pin"
                size={hp("2.5")}
                color={color.primary}
              />
            }
            placeholder={"Province (From)"}
          />
          <View>
            <TouchableOpacity
              style={{
                position: "relative",
                top: hp("1.5"),
                alignSelf: "flex-end",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: color.secondary,
                width: hp("5"),
                height: hp("5"),
                borderRadius: hp("5") / 2,
              }}
            >
              <MaterialIcons name="swap-vert" size={hp("2.5")} color={"#fff"} />
            </TouchableOpacity>
          </View>

          <Text style={styles.topic}>To</Text>
          <TextInput
            leftIcon={
              <Entypo
                name="location-pin"
                size={hp("2.5")}
                color={color.primary}
              />
            }
            placeholder={"Province (To)"}
          />
          <View style={{ marginBottom: hp("2") }} />
          <View style={{ flexDirection : "row" , alignItems : "center" , justifyContent : "space-between" }}>
            <Text style={styles.topic}>Depart Date</Text>

            <Switch title={"Round-trip"}  value={roundTrip} onChange={() => roundTrip ? setRoundTrip(false) : setRoundTrip(true) } />
          </View>
          <DateTimePicker
            placeholder={"Date Time"}
            leftIcon={
              <FontAwesome
                name="calendar"
                size={hp("2.5")}
                color={color.primary}
              />
            }
            data={departDate}
            handleOnChange={handleOnSelectDepartDate}
          />
          <View style={{ marginBottom: hp("2") }} />
          <View>
            <Text style={styles.topic}>No of Passenger</Text>
          </View>
          <TextInput
            defaultValue={noPassenger.toString()}
            textUnit={"Person"}
            keyboardType={"numeric"}
            placeholder={""}
            leftIcon={
              <FontAwesome name="user" size={hp("2.5")} color={color.primary} />
            }
          />

          <View style={{ marginBottom: hp("20") }} />
          <Button
            onPress={handleOnSearch}
            backgroundColor={color.primary}
            style={{ alignSelf: "center", marginBottom: hp("4"), width: "95%" }}
          >
            <Text style={{ color: "#fff" }}>Search</Text>
          </Button>
        </ScrollView>
      </KeyboardAvoidingView>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: hp("2.5"),
    paddingBottom: hp("5"),
    alignSelf: "center",
    width: "90%",
  },
  topic: { fontSize: hp("2.2"), color: "#696969", marginBottom: hp("1") },
});
