import React, { useState } from "react";
import { StyleSheet, View, ScrollView, TouchableOpacity , FlatList } from "react-native";
import { MaterialIcons, Entypo, FontAwesome , AntDesign } from "@expo/vector-icons";
import { color } from "../../Utils";

import {
  DateTimePicker,
  Container,
  KeyboardAvoidingView,
  Header,
  Text,
  TextInput,
  TermConditon,
  Button,
  Picker,
  RadioBtn,
  Switch,
} from "../../Components";

import {
    TicketPromotion,
    TicketNormal
} from "../../Components/Search";


import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { t } from "i18n-js";

export default function TicketBackScreen({ navigation }) {
  // State
  const [fromState, setFromState] = useState("From");
  const [toState, setToState] = useState("To");

  return (
    <Container>
      <Header
        title={
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontSize : hp("3") }}>
              {fromState}{" "}
              <AntDesign name="arrowright" size={hp("3")} color="black" />{" "}
              {toState}
            </Text>
            <Text style={{ fontSize: hp("2.2") }}>Select Return Trip</Text>
          </View>
        }
      />
      
      <FlatList
        contentContainerStyle={{
            paddingTop : hp("3"),
            alignSelf : "center",
            width  : "95%"
        }}
        data={[{ type :  "p" , data : 1 },{ type :  "p" , data : 1 },{ type :  "n" , data : 1 }]}
        renderItem={({item}) => {

              if(item.type == "p"){
                 return <TicketPromotion />
              }else{
                 return <TicketNormal />
              }
                
        }}
        keyExtractor={(item,index) => index.toString()}
      />


    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: hp("2.5"),
    paddingBottom: hp("5"),
    alignSelf: "center",
    width: "90%",
  },
  topic: { fontSize: hp("2.2"), color: "#696969", marginBottom: hp("1") },
});
