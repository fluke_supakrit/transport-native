import React, { useState } from "react";
import { StyleSheet, View, Image } from "react-native";
import { t } from "i18n-js";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

import { Text, Container } from "../../Components";
import { color } from "../../Utils";
import AppIntroSlider from "react-native-app-intro-slider";

export default function IntroScreen() {
  const [slides, setSlides] = useState([
    {
      title: t("Intro_step_1"),
      image: require('../../../assets/images/intro/guideline1.png'),
      cardTitle: t("Intro_step_1_card_title"),
      cardBody: t("Intro_step_1_card_body"),
    },
    {
      title: t("Intro_step_2"),
      image: require('../../../assets/images/intro/guideline2.png'),
      cardTitle: t("Intro_step_2_card_title"),
      cardBody: t("Intro_step_2_card_body"),
    },
    {
      title: t("Intro_step_3"),
      image: require('../../../assets/images/intro/guideline3.png'),
      cardTitle: t("Intro_step_3_card_title"),
      cardBody: t("Intro_step_3_card_body"),
    },
    {
      title: t("Intro_step_4"),
      image: require('../../../assets/images/intro/guideline4.png'),
      cardTitle: t("Intro_step_4_card_title"),
      cardBody: t("Intro_step_4_card_body"),
    },
    {
      title: t("Intro_step_5"),
      image: require('../../../assets/images/intro/guideline5.png'),
      cardTitle: t("Intro_step_5_card_title"),
      cardBody: t("Intro_step_5_card_body"),
    },
  ]);

  const renderItem = ({ item }) => {
    return (
      <View key={item.title} style={styles.slide}>
        <View style={styles.step}>
          <Text style={{ color: "#fff", fontWeight: "bold" }}>
            {item.title}
          </Text>
        </View>
        <View style={styles.slideBody}>
          <Image
            source={item.image}
            style={{
              width: "100%",
              height: "100%",
            }}
          />

          <View style={styles.card}>
            <Text style={styles.cardTitle}>{item.cardTitle}</Text>
            <View style={styles.cardBody}>
              <Text style={{ fontSize: hp("2.3"), textAlign: "center" }}>
                {item.cardBody}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  const onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({ showRealApp: true });
  };

  return (
    <Container>
      <View
        style={{
          padding: hp("5"),
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text
          style={{
            color: color.primary,
            fontSize: hp("3"),
            fontWeight: "bold",
          }}
        >
          {t("Intro_guide")}
        </Text>
      </View>

      <AppIntroSlider
        renderItem={renderItem}
        data={slides}
        onDone={onDone}
        doneLabel={"เสร็จ"}
        nextLabel={"ต่อไป"}
        keyExtractor={(val, index) => index.toString()}
      />
    </Container>
  );
}

const styles = StyleSheet.create({
  slide: {
    flex: 1,
  },
  step: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    width: hp("20"),
    borderRadius: hp("5"),
    padding: hp("0.6"),
    backgroundColor: color.primary,
  },

  slideBody: {
    marginTop: hp("5"),
    width: "100%",
    height: hp("50"),
  },

  card: {
    padding: hp("1"),
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    zIndex: 2,
    bottom: 0,
  },
  cardTitle: {
    fontSize: hp("3.5"),
  },
  cardBody: {
    marginTop: hp("2"),
    width: wp("95"),
  },
});
