import React, { useState } from "react";
import { StyleSheet, View, ScrollView } from "react-native";
import {
  Zocial,
  Ionicons,
  FontAwesome,
  Foundation,
  Entypo,
} from "@expo/vector-icons";
import { color } from "../../Utils";
import {
  DateTimePicker,
  Container,
  KeyboardAvoidingView,
  Header,
  Text,
  TextInput,
  TermConditon,
  Button,
  Picker,
  RadioBtn,
} from "../../Components";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { t } from "i18n-js";

export default function RegisterScreen({ navigation }) {
  // State
  const [selectedGender, setSelectedGender] = useState({
    label: t("Register_Male"),
    value: 0,
  });
  const [gender, setGender] = useState([
    { label: t("Register_Male"), value: 0 },
    { label: t("Register_Female"), value: 1 },
  ]);

  const [selectedIdCardList, setSelectedIdCardList] = useState({
    label: t("Register_IdentificationNumber"),
    value: 0,
  });
  const [idCardList, setIdCardList] = useState([
    { label: t("Register_IdentificationNumber"), value: 0 },
    { label: t("Register_PassportNumber"), value: 1 },
  ]);
  const [dateBirth, setDateBirth] = useState("");
  const [acceptTermCons, setAcceptTermCons] = useState(false)

  // Event
  const handleOnSelectGender = (obj) => {
    setSelectedGender(obj);
  };

  const handleOnSelectIdCardList = (obj) => {
    setSelectedIdCardList(obj);
  };

  const handleOnSelectDateBirth = (selectedDate) => {
    console.log("selectedDate => ", selectedDate);
    setDateBirth(selectedDate);
  };

  const handleAcceptTermCons = () => {
      acceptTermCons ? setAcceptTermCons(false) : setAcceptTermCons(true)
  }

  return (
    <Container>
      <Header 
        title={t("Register_Register")} 
      />

      <KeyboardAvoidingView>
        <ScrollView>
          <View style={styles.form}>
            <Text style={styles.topic}>{t("Register_CreateYourAccount")}</Text>
            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <Zocial name="email" size={hp("2.5")} color={color.primary} />
              }
              placeholder={t("Register_Email")}
            />
            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <FontAwesome
                  name="user"
                  size={hp("2.5")}
                  color={color.primary}
                />
              }
              placeholder={t("Register_Username")}
            />
            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <Ionicons
                  name="ios-lock"
                  size={hp("2.5")}
                  color={color.primary}
                />
              }
              placeholder={t("Register_Password")}
              secureTextEntry={true}
            />
            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <Ionicons
                  name="ios-lock"
                  size={hp("2.5")}
                  color={color.primary}
                />
              }
              placeholder={t("Register_ConfirmPassword")}
              secureTextEntry={true}
            />
            <View style={{ marginBottom: hp("1.5") }} />

            <Text style={styles.topic}>
              {t("Register_PersonalInformation")}
            </Text>

            <Text style={{ marginBottom: hp("1") }}>{t("Register_Title")}</Text>
            <Picker
              data={[
                {
                  label: t("Register_prefix_mr"),
                  value: t("Register_prefix_mr"),
                },
                {
                  label: t("Register_prefix_ms"),
                  value: t("Register_prefix_ms"),
                },
              ]}
            />
            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <FontAwesome
                  name="user"
                  size={hp("2.5")}
                  color={color.primary}
                />
              }
              placeholder={t("Register_Name")}
            />
            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <FontAwesome
                  name="user"
                  size={hp("2.5")}
                  color={color.primary}
                />
              }
              placeholder={t("Register_LastName")}
            />
            <View style={{ marginBottom: hp("1.5") }} />
            <RadioBtn
              label={t("Register_Sex")}
              selected={selectedGender}
              data={gender}
              onPress={handleOnSelectGender}
            />

            <View style={{ marginBottom: hp("1.5") }} />
            <DateTimePicker
              placeholder={t("Register_DateOfBirth")}
              leftIcon={
                <FontAwesome
                  name="calendar"
                  size={hp("2.5")}
                  color={color.primary}
                />
              }
              data={dateBirth}
              handleOnChange={handleOnSelectDateBirth}
            />

            <View style={{ marginBottom: hp("1.5") }} />
            <RadioBtn
              selected={selectedIdCardList}
              data={idCardList}
              onPress={handleOnSelectIdCardList}
            />

            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <FontAwesome
                  name="id-card"
                  size={hp("2.5")}
                  color={color.primary}
                />
              }
              placeholder={selectedIdCardList.label}
            />

            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <Foundation
                  name="telephone"
                  size={hp("2.5")}
                  color={color.primary}
                />
              }
              placeholder={t("Register_Mobile")}
            />

            <View style={{ marginBottom: hp("1.5") }} />
            <TextInput
              leftIcon={
                <Entypo
                  name="location-pin"
                  size={hp("2.4")}
                  color={color.primary}
                />
              }
              placeholder={t("Register_Address")}
            />
            <View style={{ marginBottom: hp("1") }} />
            <TermConditon checked={acceptTermCons} onPress={handleAcceptTermCons} />
            <View style={{ marginBottom: hp("1.5") }} />
            <Button
              disabled={!acceptTermCons}
              backgroundColor={color.primary}
            >
              <Text style={{ color : "#fff" }}>{t("Register_Register")}</Text>
              </Button>

          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </Container>
  );
}

const styles = StyleSheet.create({
  form: {
    marginTop: hp("2.5"),
    marginBottom: hp("12"),
    alignSelf: "center",
    width: "90%",
  },

  topic: { fontSize: hp("2.2"), color: "#696969", marginBottom: hp("1") },
});
