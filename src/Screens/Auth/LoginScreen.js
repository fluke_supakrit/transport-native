import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Ionicons, FontAwesome } from "@expo/vector-icons";
import { color } from "../../Utils";
import { Container, Header, Text, TextInput, Button } from "../../Components";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

import { t } from "i18n-js";

export default function Login({ navigation }) {
  return (
    <Container>
      <Header title={t("Login_login")} />
      <View style={styles.form}>
        <TextInput
          leftIcon={
            <FontAwesome name="user" size={hp("2.5")} color={color.primary} />
          }
          placeholder={t("Login_username")}
        />
        <View style={{ marginBottom: hp("2.5") }} />
        <TextInput
          leftIcon={
            <Ionicons name="ios-lock" size={hp("2.5")} color={color.primary} />
          }
          placeholder={t("Login_password")}
        />
        <View style={{ marginBottom: hp("2.5") }} />

        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <TouchableOpacity>
            <Text style={{ fontSize: hp("2"), color: "#696969" }}>
              {t("Login_forgotPassword")}
            </Text>
          </TouchableOpacity>
          <Text> </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate("RegisterScreen")}
          >
            <Text style={{ fontSize: hp("2"), color: "#696969" }}>
              {t("Login_register")}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{ marginBottom: hp("2.5") }} />
        <Button
          backgroundColor={color.primary}
        >
           <Text style={{ color : "#fff" }}>{t("Login_login")}</Text>
        </Button>
        <View style={{ marginBottom: hp("2.5") }} />
        <Button
          onPress={() => navigation.navigate("HomeScreen")}
        >
          <Text style={{ color : color.text }}>{t("Login_skip")}</Text>
        </Button>
      </View>
    </Container>
  );
}
const styles = StyleSheet.create({
  form: {
    alignSelf: "center",
    alignItems: "center",
    marginTop: hp("4"),
    width: "90%",
  },
});
