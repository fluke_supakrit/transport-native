import React from "react";
import { StyleSheet, View, Image, ScrollView , Platform } from "react-native";
import { Header, Container, Text, Card } from "../../Components";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function Contact() {
  return (
    <Container>
      <Header title="Contact Us" />

      <ScrollView>
        <View style={styles.container}>
          <Text
            style={{
              marginBottom: hp("1"),
              marginTop: hp("2"),
              fontSize: hp("2"),
            }}
          >
            Telephone Number
          </Text>
          <Card style={{ padding: hp("2") }}>
            <View style={{ flexDirection: "row" }}>
              <View style={ Platform.OS === "ios" ? { padding: 0 } : { padding: hp("1") } }>
                <Image
                  source={require("../../../assets/images/contact/bullets.png")}
                  style={{
                    width: wp("7.2"),
                    height: hp("70"),
                  }}
                  resizeMode={"contain"}
                />
              </View>

              <View style={{ marginLeft: wp("6") }}>
                <View style={{ marginBottom: hp("1.8") }} />
                <Text style={{ fontWeight: "bold", marginBottom: hp("1") }}>
                  Northern Thailand
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>
                  02-936-2852-66 ต่อ 614, 325
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>
                  02-936-2841-48 ต่อ 614, 325
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>02-936-3670</Text>

                <View style={{ marginBottom: hp("3.5") }} />

                <Text style={{ fontWeight: "bold", marginBottom: hp("1") }}>
                  Easthern Thailand
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>02-391-6846</Text>

                <View style={{ marginBottom: hp("9") }} />

                <Text style={{ fontWeight: "bold", marginBottom: hp("1") }}>
                  Southern Thailand (From Mo Chit)
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>02-537-0291</Text>

                <View style={{ marginBottom: hp("9") }} />

                <Text style={{ fontWeight: "bold", marginBottom: hp("1") }}>
                  Southern Thailand
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>02-422-4444</Text>

                <View style={{ marginBottom: hp("9") }} />

                <Text style={{ fontWeight: "bold", marginBottom: hp("1") }}>
                  Northeasthern Thailand
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>
                  02-936-2852-66 ต่อ 605,602
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>
                  02-936-2841-48 ต่อ 605,602
                </Text>
                <Text style={{ fontSize: hp("2.2") }}>02-936-0657</Text>
              </View>
            </View>
          </Card>
        </View>
      </ScrollView>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    alignSelf: "center",
    width: "96%",
  },
});
