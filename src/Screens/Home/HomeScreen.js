import React from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import {
  Container,
  Header,
  Text,
  Card,
  Button,
  BottomTabNav,
  Carousel,
  CarouselOne,
} from "../../Components";
import { FontAwesome } from "@expo/vector-icons";
import { t } from "i18n-js";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { color } from "../../Utils";

export default function HomeScreen({ navigation }) {
  return (
    <Container>
      <Header
        left={
          <TouchableOpacity style={{ flexDirection: "row", padding: hp("1") }}>
            <FontAwesome
              name="user-circle-o"
              size={hp(3.5)}
              color={color.primary}
            />
            <Text>Login</Text>
          </TouchableOpacity>
        }
        title={
          <View style={{ alignSelf: "center" }}>
            <Image
              source={require("../../../assets/appIcon.png")}
              style={{
                width: wp("18.5"),
                height: hp("8"),
                position: "relative",
                bottom: hp("0.5"),
              }}
              resizeMode={"contain"}
            />
          </View>
        }
      />
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container} scol>
          <View style={{ marginBottom: hp("4") }} />
          <Card
            style={{ padding: wp("1"), flexDirection: "row", flexWrap: "wrap" }}
          >
            <View style={[styles.serviceContainer]}>
              <TouchableOpacity style={styles.serviceItem}
                onPress={() => navigation.navigate("SearchScreen")}
              >
                <Image
                  source={require("../../../assets/images/home/Search.png")}
                  style={{
                    width: hp("12"),
                    height: hp("12"),
                    position: "relative",
                  }}
                />
                <Text style={styles.serviceText}>Search</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.serviceContainer]}>
              <TouchableOpacity style={styles.serviceItem}>
                <Image
                  source={require("../../../assets/images/home/CheckBookingStatus.png")}
                  style={{
                    width: hp("12"),
                    height: hp("12"),
                    position: "relative",
                  }}
                />
                <Text style={styles.serviceText}>Check booking status</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.serviceContainer]}>
              <TouchableOpacity style={styles.serviceItem}>
                <Image
                  source={require("../../../assets/images/home/ChangeYourBooking.png")}
                  style={{
                    width: hp("12"),
                    height: hp("12"),
                    position: "relative",
                  }}
                />
                <Text style={styles.serviceText}>Change your booking</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.serviceContainer]}>
              <TouchableOpacity style={styles.serviceItem}>
                <Image
                  source={require("../../../assets/images/home/WhereAreTheyLocated.png")}
                  style={{
                    width: hp("12"),
                    height: hp("12"),
                    position: "relative",
                  }}
                />
                <Text style={styles.serviceText}>Where are they located?</Text>
              </TouchableOpacity>
            </View>
          </Card>

          <View style={{ marginBottom: hp("3") }} />

          <Card style={{ padding: hp("2") }}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <MaterialCommunityIcons
                name="bell"
                size={hp("3")}
                color={color.secondary}
              />
              <Text style={{ fontSize: hp("2") }}>
                Joined Member will receive Special Privilegs
              </Text>
            </View>
            <View style={{ marginBottom: hp("2") }} />
            <Button
              backgroundColor={color.secondary}
              style={{ padding: hp("1") }}
            >
              <Text style={{ fontSize: hp("2.4"), color: "#fff" }}>
                Sign in / Register
              </Text>
            </Button>
          </Card>

          <View style={{ marginBottom: hp("2") }} />
          <Carousel
            data={[
              { text: "Most Popular Trip" },
              { text: "Chiang Mai" },
              { text: "Kanchanaburi" },
              { text: "Korat" },
            ]}
          />
          <View style={{ marginBottom: hp("4") }} />

          <View>
            <View
              style={{
                marginBottom : hp("0.4"),
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Text style={{ fontSize: hp("3") }}>News</Text>

              <TouchableOpacity>
                <Text style={{ fontSize: hp("2.4"), color: color.primary }}>
                  Read more
                </Text>
              </TouchableOpacity>
            </View>
            <CarouselOne
              data={[
                { text: "Test" },
                { text: "Test" },
                { text: "Test" },
                { text: "Test" },
              ]}
            />
          </View>
        </ScrollView>
      <BottomTabNav />
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
  
    width: "98%",
    alignSelf : "center"
   
  },
  serviceContainer: {
    // backgroundColor: "green",
    width: "50%",
  },
  serviceItem: {
    justifyContent: "center",
    alignItems: "center",
    padding: hp("1"),
  },
  serviceText: {
    marginTop: hp("1"),
    fontSize: hp("2"),
  },
});
