import React from "react";
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Platform,
} from "react-native";
import RNPickerSelect from "react-native-picker-select";
import { FontAwesome5 } from "@expo/vector-icons";
import { font,color } from "../Utils";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function TextInputComponent({ data, style }) {
  return (
    <>
      {/* <TouchableOpacity style={styles.container}>
                <TextInput defaultValue={"Picker val"} editable={false} style={[styles.textInput,style]}  />
              
              </TouchableOpacity> */}
      <RNPickerSelect
        useNativeAndroidPickerStyle={false}
        Icon={() => (
          <FontAwesome5
            name="caret-down"
            size={hp("2.5")}
            color={color.primary}
          />
        )}
        style={{
          ...pickerSelectStyles,
          iconContainer: {
            top: wp("3"),
            right: wp("5"),
          },
        }}
        onValueChange={(value) => console.log(value)}
        items={data}
      />
    </>
  );
}

const styles = StyleSheet.create({
  textInput: {
    fontFamily: "Tahoma",
    width: "90%",
    fontSize: hp("2.4"),
    paddingLeft: wp("3"),
    color: color.text,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    padding: hp("2"),
    width: "100%",
    borderRadius: hp("0.5"),
    borderWidth: 0.5,
    borderColor: "grey",
    backgroundColor: "#fff",
    ...font.normal
  },
  inputAndroid: {
    padding: hp("1.5"),
    width: "100%",
    borderRadius: hp("0.5"),
    borderWidth: 0.5,
    borderColor: "grey",
    backgroundColor: "#fff",
    ...font.normal
  },
});
