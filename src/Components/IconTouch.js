
import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import Text from './Text'

  import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from "react-native-responsive-screen";


  export default function IconTouch({ onPress , children , style }) {
      return (
            <>
              <TouchableOpacity onPress={onPress} style={[styles.touchable,style]}>
                    <Text>
                        {children}
                    </Text>
              </TouchableOpacity>
            </>
      )
  }

  const styles = StyleSheet.create({

    touchable : {
        // backgroundColor : "red",
        alignItems : "center",
        justifyContent : "center",
        width : hp("6"),
        height : hp("6"),
        borderRadius : hp("6")/2
    }
  })
  