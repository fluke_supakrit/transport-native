import React from "react";
import { StyleSheet, View } from "react-native";
import { font , color } from "../Utils"
import Text from "./Text";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from "react-native-simple-radio-button";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function RadioBtn({ label , selected, onPress, data }) {
  return (
    <View>
     
      <RadioForm formHorizontal={true} animation={true} initial={0}>
        <Text style={{ paddingRight : wp("1") }}>
          {label}
        </Text>
        {data.length ? (
          data.map((obj, i) => (
            <RadioButton
              style={{
                justifyContent: "space-between",
              }}
              key={i}
            >
              <RadioButtonInput
                obj={obj}
                index={i}
                isSelected={selected.value === i}
                onPress={() => onPress(obj)} // onClick Radio
                borderWidth={1}
                buttonInnerColor={color.primary}
                buttonOuterColor={"#707070"}
                buttonSize={hp("1.5")}
                buttonOuterSize={hp("3")}
                buttonStyle={{}}
                // buttonWrapStyle={{marginLeft: 10}}
              />
              <RadioButtonLabel
                obj={obj}
                index={i}
                onPress={() => onPress(obj)} // onClick Label
                labelStyle={{
                  paddingLeft: wp("1.5"),
                  paddingRight: wp("3"),
                  ...font.normal,
                  fontSize : hp("2.3")
                }}
                labelWrapStyle={{}}
              />
            </RadioButton>
          ))
        ) : (
          <Text>Radio no data</Text>
        )}
      </RadioForm>
    </View>
  );
}

const styles = StyleSheet.create({});
