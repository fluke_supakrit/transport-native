import React, { useState } from "react";
import { StyleSheet , Dimensions, View, Image } from "react-native";
import Carousel from 'react-native-snap-carousel'
import Text from "./Text";
import Card from "./Card";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function CarouselComponnt({ onPress, data , style }) {
  const SLIDER_WIDTH = Dimensions.get("window").width;
  const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.32);
  let [_carousel, setCarousel] = useState();
  return (
    <>
      <Carousel
        contentContainerCustomStyle={{
          overflow: "hidden",
          width: ITEM_WIDTH * data.length + wp("8"),
        }}
        layout={"default"}
        ref={(c) => {
          setCarousel(c);
        }}
        activeSlideAlignment={"start"}
        data={data}
        renderItem={({ item }) => {
          return (
                <View>
                     <Card style={{ height : hp("15") }}>
                        <Image
                            source={require("../../assets/appIcon.png")}
                            style={{  width : "100%",
                            height : "100%"}}
                            resizeMode="cover"
                        />
                    </Card>
                    <Text style={{ alignSelf: 'center', fontSize : hp("2") }}> {item.text} </Text>
                </View>
          );
        }}
        inactiveSlideOpacity={1}
        inactiveSlideScale={1}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
      />
    </>
  );
}

const styles = StyleSheet.create({
  carousel: {},
});
