import React, { useState } from "react";
import { StyleSheet, Dimensions, View, Image } from "react-native";
import Carousel from "react-native-snap-carousel";
import Text from "./Text";
import Card from "./Card";
import { color } from "../Utils";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function CarouselOneComponnt({ onPress, data, style }) {
  const SLIDER_WIDTH = Dimensions.get("window").width;
  const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.95);
  let [_carousel, setCarousel] = useState();
  return (
    <>
      <Carousel
        layout={"default"}
        ref={(c) => {
          setCarousel(c);
        }}
        activeSlideAlignment={"start"}
        data={data}
        renderItem={({ item }) => {
          return (
            <View>
              <Card style={{ height: hp("25") }}>
                <Image
                  source={require("../../assets/appIcon.png")}
                  style={{ width: "100%", height: "100%" }}
                  resizeMode="center"
                />

                <View
                  style={{
                    top: hp("3"),
                    right: 0,   
                    backgroundColor: "red",
                    borderTopLeftRadius: hp("4"),
                    borderBottomLeftRadius: hp("4"),
                    position: "absolute",
                    backgroundColor: color.secondary,
                    padding: hp("1.5"),
                  }}
                >
                  <View style={{ width : wp("20") , alignItems : "center" }}>
                    <Text style={{ fontSize: hp("2.3"), color: "#fff" }}>
                      Search
                    </Text>
                  </View>
                </View>
              </Card>
            </View>
          );
        }}
        inactiveSlideOpacity={1}
        inactiveSlideScale={1}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
      />
    </>
  );
}

const styles = StyleSheet.create({
  carousel: {},
});
