import React, { useState } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import {
  Entypo,
  MaterialCommunityIcons,
  Ionicons,
  FontAwesome,
} from "@expo/vector-icons";
import Text from "./Text";
import { font, color } from "../Utils";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function ButtonComponent() {
  const [menu1Active, setMenu1Active] = useState(false);
  const [menu2Active, setMenu2Active] = useState(false);
  const [menu3Active, setMenu3Active] = useState(false);
  const [menu4Active, setMenu4Active] = useState(false);

  const clearActive = () => {
    setMenu1Active(false);
    setMenu2Active(false);
    setMenu3Active(false);
    setMenu4Active(false);
  };

  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity
          style={{ alignItems: "center" }}
          onPress={() => {
            clearActive();
            setMenu1Active(true);
          }}
        >
          <Entypo
            name="home"
            size={hp("3")}
            color={menu1Active ? color.primary : "#adadaf"}
          />
          <Text
            style={
              menu1Active
                ? { ...styles.text, color: color.primary }
                : styles.text
            }
          >
            Home
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{ alignItems: "center" }}
          onPress={() => {
            clearActive();
            setMenu2Active(true);
          }}
        >
          <MaterialCommunityIcons
            name="bag-checked"
            size={hp("3")}
            color={menu2Active ? color.primary : "#adadaf"}
          />
          <Text
            style={
              menu2Active
                ? { ...styles.text, color: color.primary }
                : styles.text
            }
          >
            My booking
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{ alignItems: "center" }}
          onPress={() => {
            clearActive();
            setMenu3Active(true);
          }}
        >
          <Ionicons
            name="ios-gift"
            size={hp("3")}
            color={menu3Active ? color.primary : "#adadaf"}
          />
          <Text
            style={
              menu3Active
                ? { ...styles.text, color: color.primary }
                : styles.text
            }
          >
            Privilegs
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{ alignItems: "center" }}
          onPress={() => {
            clearActive();
            setMenu4Active(true);
          }}
        >
          <FontAwesome
            name="user"
            size={hp("3")}
            color={menu4Active ? color.primary : "#adadaf"}
          />
          <Text
            style={
              menu4Active
                ? { ...styles.text, color: color.primary }
                : styles.text
            }
          >
            Account
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
    height: hp("10"),
    backgroundColor: "#fff",
    paddingTop: hp("1"),

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
  },
  text: {
    fontSize: hp("2.2"),
    color: "#adadaf",
  },
});
