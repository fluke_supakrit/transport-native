import React, { useState, useRef } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  TextInput,
  Platform,
  Text,
} from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import RBSheet from "react-native-raw-bottom-sheet";

import moment from "moment";
import { font, color } from "../Utils";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function DateTimePickerComponent({
  leftIcon,
  placeholder,
  data,
  style,
  handleOnChange,
}) {
  const RBSheetRef = useRef();
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    setShow(false);
    setShow(Platform.OS === "ios");
    if (selectedDate) {
      handleOnChange(selectedDate);
    }
  };

  return (
    <>
      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          if (Platform.OS === "ios") {
            setShow(true);
            RBSheetRef.current.open();
          } else {
            setShow(true);
          }
        }}
      >
        <View style={styles.icon}>{leftIcon}</View>
        <Text style={[styles.textInput, style]}>
          {data ? moment(data).format("DD/MM/YYYY") : placeholder}
        </Text>
      </TouchableOpacity>

      {show && Platform.OS != "ios" ? (
        <DateTimePicker
          testID="dateTimePicker"
          value={data ? data : new Date()}
          mode={"date"}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      ) : (
        <RBSheet
          ref={RBSheetRef}
          height={hp("35")}
          customStyles={{
            wrapper: {
              backgroundColor: "transparent",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 12,
              },
              shadowOpacity: 0.58,
              shadowRadius: 16.0,

              elevation: 24,
            },
            draggableIcon: {
              backgroundColor: "#ccc",
            },
          }}
        >
          <View style={{ width: "100%" }}>
            <TouchableOpacity
              style={{ alignSelf: "flex-end", padding: hp("1") }}
              onPress={() => RBSheetRef.current.close()}
            >
              <Text style={{ fontSize: hp("2.5"), fontWeight: "bold" }}>
                Done
              </Text>
            </TouchableOpacity>
            <DateTimePicker
              testID="dateTimePicker"
              value={data ? data : new Date()}
              mode={"date"}
              is24Hour={true}
              display="default"
              onChange={onChange}
            />
          </View>
        </RBSheet>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderRadius: hp("0.5"),
    borderWidth: 0.5,
    borderColor: "grey",
    backgroundColor: "#fff",
  },
  icon: {
    padding: hp("2"),
  },

  textInput: {
    ...font.normal,
    width: "90%",
  },
});
