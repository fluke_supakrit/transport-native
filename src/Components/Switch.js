import React from "react";

import { StyleSheet, View , Switch } from "react-native";
import Text from "./Text";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function SwitchComponent({ title, value, onChange, style }) {
  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        margin: hp("0.4"),
      }}
    >
      <Text style={styles.topic}> {title} </Text>
      <Switch
          style={style}
          onValueChange={onChange}
          value={value}
        />
    </View>
  );
}

const styles = StyleSheet.create({

    topic: { fontSize: hp("2.2"), color: "#696969", marginBottom: hp("1") },
});
