import React, { useState } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Modal from "react-native-modal";
import { Entypo, Ionicons, Fontisto } from "@expo/vector-icons";

import { SafeAreaView } from "react-native-safe-area-context"

import Text from "./Text";
import IconTouch from "./IconTouch";
import { color } from "../Utils";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function HeaderComponent({ left, backgroundColor, title }) {
  const navigation = useNavigation();

  // State
  const [actionMenuOpen, setActionMenuOpen] = useState(false);

  const renderTitle = (title) => {
    if (typeof title === "object") {
      return title;
    }
    return <Text>{title}</Text>;
  };

  const renderLeft = (left) => {
    if (typeof left === "object") {
      return left;
    }
    return (
      <IconTouch onPress={() => navigation.goBack()}>
        <Ionicons name="ios-arrow-back" size={hp("4")} color="black" />
      </IconTouch>
    );
  };

  return (
    <>
      <View
        style={[
          styles.header,
          { backgroundColor: backgroundColor ? backgroundColor : "#fff" },
        ]}
      >
        <View style={{ flex: 1, alignItems: "flex-start" }}>
          {renderLeft(left)}
        </View>

        <View style={{ flex: 2, alignItems: "center" }}>
          {renderTitle(title)}
        </View>
        <View style={{ flex: 1, alignItems: "flex-end" }}>
          <IconTouch onPress={() => setActionMenuOpen(true)}>
            <Entypo name="dots-three-horizontal" size={hp("4")} color="black" />
          </IconTouch>
        </View>
      </View>

      <Modal
        style={styles.modal}
        animationIn="slideInDown"
        animationOut="slideOutUp"
        onBackdropPress={() => setActionMenuOpen(false)}
        isVisible={actionMenuOpen}
      >
        <SafeAreaView style={styles.actionMenuContainer}>
          <View style={{ borderBottomWidth: 0.5, borderColor: "black" }}>
            <TouchableOpacity
              style={{ alignSelf: "flex-end", padding: hp("2") }}
              onPress={() => setActionMenuOpen(false)}
            >
              <Text style={styles.actionMenuText}>close</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              padding: hp("2"),
              justifyContent: "center",
              borderBottomWidth: 0.6,
              borderColor: "black",
            }}
          >
            <TouchableOpacity
              style={{ alignItems: "center", padding: hp("1") }}
            >
              <Text style={styles.actionMenuText}>Help</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ alignItems: "center", padding: hp("1") }}
            >
              <Text style={styles.actionMenuText}>How to buy</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ alignItems: "center", padding: hp("1") }}
              onPress={() => {
                  navigation.navigate("ContactScreen")
                  setActionMenuOpen(false)
              }}
            >
              
              <Text style={styles.actionMenuText}>Contact us</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ alignItems: "center", padding: hp("1") }}
            >
              <Text style={styles.actionMenuText}>Setting</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              padding: hp("1"),
              justifyContent: "center",
            }}
          >
            <TouchableOpacity
              style={{ alignItems: "center", padding: hp("1") }}
            >
              <Text style={styles.actionMenuText}>
                <Fontisto name="world-o" size={hp("2.7")} color="#fff" /> Thai
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ alignItems: "center", padding: hp("1") }}
            >
              <Text style={styles.actionMenuText}>English</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#fff",
    height: hp("10"),
    paddingLeft: wp("2"),
    paddingRight: wp("2"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },

  modal: {
    flex: 1,
    margin: 0,
    justifyContent: "flex-start",
  },
  actionMenuContainer: {
    backgroundColor: color.primary,
  },
  actionMenuText: {
    color: "#fff",
    fontSize: hp("2.4"),
  },
});
