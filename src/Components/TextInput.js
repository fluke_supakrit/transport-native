import React from "react";
import { StyleSheet, TextInput, View } from "react-native";
import { color } from "../Utils";
import Text from "./Text"
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function TextInputComponent({
  defaultValue,
  leftIcon,
  placeholder,
  style,
  secureTextEntry,
  onChangeText,
  keyboardType,
  textUnit
}) {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.icon}>{leftIcon}</View>
        <TextInput
          defaultValue={defaultValue}
          keyboardType={keyboardType ? keyboardType : "default"}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          placeholder={placeholder}
          placeholderTextColor={color.text}
          style={ textUnit ? [ styles.textInput, {  width: "10%" } , style ] : [styles.textInput,{  width: "85%" } , style]}
        />

        {textUnit ? <Text> {textUnit} </Text> : false }

      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderRadius: hp("0.5"),
    borderWidth: 0.5,
    borderColor: "grey",
    backgroundColor: "#fff",
  },
  icon: {
    padding: hp("2"),
  },
  textInput: {
    fontFamily: "Tahoma",
    fontSize: hp("2.4"),
    color: "#152238",
  },
});
