import React, { useState } from "react";
import { useNavigation } from "@react-navigation/native"
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Platform,
  FlatList,
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome5";
import { Text } from "../../Components";
import { color } from "../../Utils";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function TicketPromotion() {

  const navigation = useNavigation()

  return (
    <View>
      <TouchableOpacity style={styles.container} activeOpacity={0.8} onPress={() => navigation.navigate("TicketBackScreen")} > 
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Image
              source={{
                uri:
                  "https://s.isanook.com/mn/0/rp/r/w728/ya0xa0m1w0/aHR0cHM6Ly9zLmlzYW5vb2suY29tL21uLzAvdWQvODkvNDQ4NjAxLzc1MjQ0MC0wMS5qcGc=.jpg",
                width: wp("22"),
                height: hp("12"),
              }}
              resizeMode="contain"
            />
          </View>
          <View
            style={{
              flexDirection: "column",
              paddingTop: hp("3"),
              paddingBottom: hp("3"),
            }}
          >
            <Text style={{ fontSize: hp("1.7"), color: color.grey }}>Time</Text>
            <Text style={{ fontSize: hp("3"), color: "#000" }}>08.30 AM</Text>
            <Text style={{ fontSize: hp("1.7"), color: "#000" }}>
              No. of seats (available 37)
              <Text style={{ fontSize: hp("1.7"), color: color.grey }}>
                Standard ม.4ข
              </Text>
            </Text>
          </View>
          <View
            style={{ borderLeftWidth: 1, height: "70%", borderColor: "#ccc" }}
          />
          <View
            style={{
              width: Platform.OS == "ios" ? "15%" : "20%",
              alignItems: "center",
            }}
          >
            <Text style={{ color: color.grey, fontSize: hp("2") }}>Price</Text>
            <Text style={{ color: color.primary, fontSize: hp("3.2") }}>
              250
            </Text>
            <Text style={{ color: color.grey, fontSize: hp("2") }}>THB</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const showdowBox = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.1,
  shadowRadius: 1.41,

  elevation: 2,
};

const styles = StyleSheet.create({
  container: {
    // marginBottom: hp("2.5"),
    backgroundColor: "#fff",
    borderRadius: wp("1"),
    ...showdowBox,
  },

  headerBar: {
    flexDirection: "row",
    borderTopStartRadius: wp("1"),
    borderTopEndRadius: wp("1"),
    backgroundColor: "#ccc",
  },

  triangle: {
    position: "absolute",
    right: wp("-0.44"),
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderLeftWidth: hp("1.51"),
    borderRightWidth: hp("1.51"),
    borderBottomWidth: wp("5"),
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "#ccc",
    transform: [{ rotate: "-90deg" }],
    margin: 0,
    marginLeft: -6,
    borderWidth: 0,
  },

  expandMenu: {
    backgroundColor: "#fff",
    borderWidth: 0.2,
    borderRadius: hp("0.5"),
    flex: 1,
    zIndex: 5,
    alignItems: "center",
    alignSelf: "center",
    position: "relative",
    bottom: hp("1.5"),
    width: wp("30"),
    ...showdowBox,
  },

  promotionContainer: {
    borderBottomStartRadius: wp("1"),
    borderBottomEndRadius: wp("1"),
    position: "relative",
    zIndex: 4,
    backgroundColor: "#fff",
    bottom: hp("1.5"),
    ...showdowBox,
  },
});
