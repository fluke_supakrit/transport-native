
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { font , color} from '../Utils'

  import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from "react-native-responsive-screen";


  export default function TextComponent({ children , style }) {
      return (
            <>
              <Text style={[styles.text,style]}> {children} </Text>
            </>
      )
  }

  const styles = StyleSheet.create({
      
    text : font.normal
  })
  