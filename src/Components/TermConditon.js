import React from "react";
import { useSelector } from "react-redux";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { CheckBox } from "react-native-elements";
import { MaterialIcons, FontAwesome5 } from "@expo/vector-icons";

import Text from "./Text";
import { color } from "../Utils";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function TermConditon({ checked, onPress, condition }) {
  const { language } = useSelector((state) => state.lang);
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <CheckBox
        containerStyle={{
          padding: hp("0.5"),
          margin: 0,
          backgroundColor: "transparent",
        }}
        center
        checkedIcon={
          <FontAwesome5 name="check-square" size={hp("2.3")} color="black" />
        }
        uncheckedIcon={
          <MaterialIcons
            name="check-box-outline-blank"
            size={hp("2.5")}
            color="black"
          />
        }
        checked={checked}
        onPress={onPress}
      />

      {language === "th" ? (
        <>
          <Text style={{ fontSize: hp("2") }}>ฉันยอมรับ</Text>
          <TouchableOpacity>
            <Text
              style={{
                fontSize: hp("2"),
                color: color.primary,
                textDecorationLine: "underline",
              }}
            >เงื่อนไขข้อตกลง
            </Text>
          </TouchableOpacity>
          <Text style={{ fontSize: hp("2") }}>ของทาง บขส.</Text>
        </>
      ) : (
        <>
          <Text style={{ fontSize: hp("2") }}>I accept the </Text>
          <TouchableOpacity>
            <Text
              style={{
                fontSize: hp("2"),
                color: color.primary,
                textDecorationLine: "underline",
              }}
            >terms and conditions
            </Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({});
