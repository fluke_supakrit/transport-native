import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import Text from "./Text";
import { font, color } from "../Utils";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function ButtonComponent({
  children,
  backgroundColor,
  style,
  disabled,
  onPress
}) {
  return (
    <>
      <TouchableOpacity
        onPress={onPress}
        disabled={disabled}
        style={[styles.btn, { backgroundColor: backgroundColor ? backgroundColor : "#fff" }, style]}
      >
        {children}
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  text: font.normal,
  btn: {
    width: "100%",
    justifyContent : "center",
    alignItems: "center",
    borderRadius: hp("5"),
    padding: hp("1.6"),
    borderWidth: 0.3,
  },
});
