import React from "react";
import { StyleSheet, View } from "react-native";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default function CardComponent({ children, style }) {
  return <View style={[styles.card, style]}>
          {children}
        </View>;
}

const styles = StyleSheet.create({
  card: {
    margin : wp("1.5"),
    backgroundColor : "#fff",
    borderRadius : wp("1"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
});
