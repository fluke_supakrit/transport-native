import React from 'react'
import { View, Platform, StatusBar } from 'react-native'
import { colors } from './Utils'
import { Container } from './Components'

export default class AppInit extends React.Component {
    constructor(props) {
        super(props)
        this.initApplication()
    }

    initApplication() {
        this.props.navigation.navigate('App')
    }

    render() {
        return <View />
    }
}