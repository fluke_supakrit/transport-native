
import font from "./font"
import color from "./color"
import config from "./config"

export { font,color , config } 