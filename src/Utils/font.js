import color from "./color"


import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from "react-native-responsive-screen";

const fonts = {
    normal: {
        fontFamily : "Tahoma",
        fontSize : hp("2.5"),
        color : color.text
    }
}

export default fonts