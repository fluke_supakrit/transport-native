const colors = {
    primary: '#ff6600',
    secondary : '#0176c3',
    text : '#03254c',
    grey : "#696969"
}

export default colors