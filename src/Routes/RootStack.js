import React, { useState } from "react";

import { IntroScreen } from "../Screens/Intro";
import { HomeScreen, ContactScreen  } from "../Screens/Home";
import { LoginScreen , RegisterScreen } from "../Screens/Auth";
import { SearchScreen, TicketScreen, TicketBackScreen } from "../Screens/Search";

import { createDrawerNavigator } from "@react-navigation/drawer";
import { createStackNavigator } from "@react-navigation/stack";

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function RootStack() {
  return (
    <Stack.Navigator initialRouteName="LoginScreen">
      
      <Stack.Screen
        name="IntroScreen"
        component={IntroScreen}
        options={{ headerShown: false }}
        screenOptions={{ gestureEnabled: false }}
      />

      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{ headerShown: false }}
        screenOptions={{ gestureEnabled: false }}
      />
       <Stack.Screen
        name="ContactScreen"
        component={ContactScreen}
        options={{ headerShown: false }}
        screenOptions={{ gestureEnabled: false }}
      />

      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
        screenOptions={{ gestureEnabled: false }}
      />

      <Stack.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{ headerShown: false }}
        screenOptions={{ gestureEnabled: false }}
      />

      <Stack.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{ headerShown: false }}
        screenOptions={{ gestureEnabled: false }}
      />

      <Stack.Screen
        name="TicketScreen"
        component={TicketScreen}
        options={{ headerShown: false }}
        screenOptions={{ gestureEnabled: false }}
      />

      <Stack.Screen
        name="TicketBackScreen"
        component={TicketBackScreen}
        options={{ headerShown: false }}
        screenOptions={{ gestureEnabled: false }}
      />

    </Stack.Navigator>
  );
}
