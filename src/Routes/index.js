import React, { useState, useEffect } from "react";
import { useSelector } from 'react-redux';

import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

const Drawer = createDrawerNavigator();

import RootStack from "./RootStack"

// Multi Lang
import i18n from 'i18n-js';
import { en , th } from '../Lang'

function Router() {

  let language = useSelector(state => state.lang.language)

  i18n.translations = {en,th}
  i18n.locale = language;
  i18n.fallbacks = true;

  const isLoggedin = useSelector(state => state.auth.isLoggedin);

  return (
    <NavigationContainer>
         <Drawer.Navigator>
                <Drawer.Screen name="Root" component={RootStack} />
            </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default Router;
